#include <iostream>
#include <string>
using namespace std;

void info() {
   cout << "\n";
   cout << "Dit programma formateerd een gegeven c++ bestand." << "\n";
   cout << "Gemaakt door:" << "\n";
   cout << "Justin Slingerland, s3162001, eerstejaars student Informatica" << "\n";
   cout << "& Jona Splinter, s3161900, eerstejaars student Informatica" << "\n";
   cout << "Beide heren hebben hun studie aan Leiden gestart in 2021." << "\n";
   cout << "Deze test is gemaakt voor de tweede praktische opdracht voor Programeer methoden." << "\n";
   cout << "\n";
   cout << "Geef eerst het invoerbestand, gewenste tab-groote, "; 
   cout << "naam van uitvoerbestand en een 3-letter-combinatie." << "\n";
   cout << "\n";
   cout << "Wanneer het programma de benodigde informatie heeft, ";
   cout << "verwijdert het al het commentaar, springt het alle regels in op de gewenste afstand," << "\n";
   cout << "telt het aantal instanties van de gegeven 3-letter-combinatie ";
   cout << "en checkt het prorgramma het collatz-vermoeden voor alle getallen." << "\n";
   cout << "Wanneer het programma klaar is, wordt de uitvoer in een bestand geplaatst." << "\n";
   cout << endl;
}

int main() {

   info();

   //benodigde variabelen
   string invoer, uitvoer;
   int tab;
   char chr1, chr2, chr3;

   //gegevens vragen
   cout << "Geef het pad van de invoerfile:" << endl;
   cin >> invoer;
   cout << "Geef de gewesnte tab-grootte: " << endl;
   cin >> tab;
   cout << "Geef het pad voor de uitvoerfile:" << endl;
   cin >> uitvoer;
   cout << "Geef een 3-letter-combinatie: " << endl;
   cin >> chr1; cin >> chr2; cin >> chr3;

   return 0;
}